describe('Adapter', () => {
  var Adapter = require('../../../src/adapter/adapter.js')

  beforeEach(() => { })

  it('should report default values', () => {
    var adapter = new Adapter(null)

    expect(adapter.getPlayhead()).toBe(null)
    expect(adapter.getPlayrate()).toBe(1)
    expect(adapter.getFramesPerSecond()).toBe(null)
    expect(adapter.getDroppedFrames()).toBe(null)
    expect(adapter.getDuration()).toBe(null)
    expect(adapter.getBitrate()).toBe(null)
    expect(adapter.getThroughput()).toBe(null)
    expect(adapter.getRendition()).toBe(null)
    expect(adapter.getTitle()).toBe(null)
    expect(adapter.getTitle2()).toBe(null)
    expect(adapter.getIsLive()).toBe(null)
    expect(adapter.getResource()).toBe(null)
    expect(adapter.getPlayerVersion()).toBe(null)
    expect(adapter.getPlayerName()).toBe(null)
    expect(adapter.getVersion()).toBeDefined()
    expect(adapter.getPosition()).toBe(null)
    expect(adapter.getLatency()).toBe(null)
    expect(adapter.getPacketLoss()).toBe(null)
    expect(adapter.getPacketSent()).toBe(null)
  })

  it('should monitor by object', () => {
    spyOn(Adapter.prototype, 'registerListeners')

    var adapter = new Adapter({})
    expect(adapter.registerListeners).toHaveBeenCalled()
  })

  it('should monitor by id', () => {
    spyOn(document, 'getElementById')

    var adapter = new Adapter('id')
    expect(document.getElementById).toHaveBeenCalled()
    expect(adapter).toBeDefined()
  })

  it('should create buffer monitor for buffer and seek', () => {
    var adapter = new Adapter(null)
    adapter.monitorPlayhead(true, true)
    expect(adapter.monitor).toBeDefined()
  })

  it('should create buffer monitor for buffer', () => {
    var adapter = new Adapter(null)
    adapter.monitorPlayhead(true, false)
    expect(adapter.monitor).toBeDefined()
  })

  it('should create buffer monitor for seek', () => {
    var adapter = new Adapter(null)
    adapter.monitorPlayhead(false, true)
    expect(adapter.monitor).toBeDefined()
  })

  it('should create buffer monitor for nothing', () => {
    var adapter = new Adapter(null)
    adapter.monitorPlayhead(false, false)
    expect(adapter.monitor).toBeDefined()
  })

  it('should dispose, with monitor', () => {
    var adapter = new Adapter({})
    adapter.monitor = {
      stop: function () { }
    }
    spyOn(adapter, 'unregisterListeners')
    spyOn(adapter.monitor, 'stop')
    adapter.dispose()
    expect(adapter.player).toBe(null)
    expect(adapter.tag).toBe(null)
    expect(adapter.unregisterListeners).toHaveBeenCalled()
    expect(adapter.monitor.stop).toHaveBeenCalled()
  })

  it('should dispose, without monitor', () => {
    var adapter = new Adapter({})
    spyOn(adapter, 'unregisterListeners')
    adapter.dispose()
    expect(adapter.player).toBe(null)
    expect(adapter.tag).toBe(null)
    expect(adapter.unregisterListeners).toHaveBeenCalled()
  })

  it('should replace player', () => {
    var player1 = {
      field1: 'value1'
    }
    var player2 = {
      field2: 'value2'
    }
    var adapter = new Adapter(player1)
    spyOn(adapter, 'unregisterListeners')
    adapter.setPlayer(player2)
    expect(adapter.player).toBe(player2)
    expect(adapter.unregisterListeners).toHaveBeenCalled()
  })
})
