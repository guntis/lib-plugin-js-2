describe('ViewTransform', () => {
  var ViewTransform = require('../../../../src/comm/transform/view')
  var vt

  beforeEach(() => {
    var plugin = {
      options: {},
      getHost: () => { return 'http://google.com' },
      adapter: {},
      getAccountCode: () => { return 'nicetest' },
      requestBuilder: {
        buildParams: function () {
          return {
            system: 'system',
            pluginVersion: '6.0.0'
          }
        }
      },
      storage: {
        setLocal: () => { }
      }
    }
    vt = new ViewTransform(plugin)

    var req = {
      getXHR: () => {
        return {
          response: '{"q":{"h":"debug-nqs-lw2.nice264.com",' +
            '"t":"","pt":"5","c":"L_19057_dytt4ca2x2j7ymj","tc":"","b":"0"}}'
        }
      }
    }
    vt._receiveData(req)
  })

  it('should parse response', () => {
    expect(vt.response.host).toBe('http://debug-nqs-lw2.nice264.com')
    expect(vt.response.code).toBe('L_19057_dytt4ca2x2j7ymj')
    expect(vt.getViewCode().length).toBe(37)
    expect(vt.response.pingTime).toBe('5')
  })

  it('should transform pings', () => {
    var ping = { service: '/ping', params: {} }
    vt.nextView()
    vt.parse(ping)
    expect(ping.host).toBe('http://debug-nqs-lw2.nice264.com')
    expect(ping.params.code.length).toBe(37)
    expect(ping.params.pingTime).toBe('5')
  })

  it('should add parameters to session stop', () => {
    var stop = { service: '/infinity/session/stop', params: {} }
    vt.setSessionId('root')
    vt.nextView()
    vt.parse(stop)
    expect(stop.params.sessionRoot).toBe('root')
  })

  it('should add parentid and navcontext in /start /init and /error ', () => {
    var start = { service: '/start', params: {} }
    var error = { service: '/error', params: {} }
    var init = { service: '/init', params: {} }
    vt._plugin = {
      getAccountCode: () => { return 'a' },
      getContext: () => { return 'DEFAULT' },
      storage: {
        getLocal: (e) => { return true }
      },
      infinity: {
        infinityStarted: true,
        isActive: function () {
          return true
        }
      }
    }
    vt.setSessionId('parent')
    vt.nextView()
    vt.parse(start)
    vt.parse(error)
    vt.parse(init)
    expect(start.params.parentId).toBe('parent')
    expect(error.params.parentId).toBe('parent')
    expect(init.params.parentId).toBe('parent')
    expect(start.params.navContext).toBe('DEFAULT')
    expect(error.params.navContext).toBe('DEFAULT')
    expect(init.params.navContext).toBe('DEFAULT')
  })

  it('should set and get sessionroot', () => {
    vt.setSessionRoot('rootString')
    expect(vt.getSessionRoot()).toBeNull()
    vt._plugin.infinity = {
      isActive: () => { return true }
    }
    expect(vt.getSessionRoot()).toBe('rootString')
  })
})
