describe('OfflineTransform', () => {
  var OfflineTransform = require('../../../../src/comm/transform/offline')
  var Transform = require('../../../../src/comm/transform/transform')

  it('should block', () => {
    var v = new OfflineTransform()
    expect(v.hasToSend()).toBe(false)
    expect(v.getState()).toBe(Transform.STATE_OFFLINE)
  })

  it('should parse', () => {
    var v = new OfflineTransform()
    v.plugin = {
      offlineStorage: {
        addEvent: function () { }
      }
    }
    spyOn(v.plugin.offlineStorage, 'addEvent')
    v.parse('request')
    expect(v.plugin.offlineStorage.addEvent).toHaveBeenCalled()
  })

  it('should not parse', () => {
    var v = new OfflineTransform()
    v.plugin = {
      offlineStorage: {
        addEvent: function () { }
      }
    }
    spyOn(v.plugin.offlineStorage, 'addEvent')
    v.parse()
    expect(v.plugin.offlineStorage.addEvent).not.toHaveBeenCalled()
  })
})
