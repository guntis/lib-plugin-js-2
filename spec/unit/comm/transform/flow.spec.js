describe('FlowTransform', () => {
  var FlowTransform = require('../../../../src/comm/transform/flow')

  it('should block until start', () => {
    var v = new FlowTransform()
    expect(v.isBlocking()).toBe(true)
    expect(v.isBlocking({ service: '/start' })).toBe(false)
    expect(v.isBlocking()).toBe(false)
  })

  it('should block until init', () => {
    var v = new FlowTransform()
    expect(v.isBlocking()).toBe(true)
    expect(v.isBlocking({ service: '/init' })).toBe(false)
    expect(v.isBlocking()).toBe(false)
  })
})
