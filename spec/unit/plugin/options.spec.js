describe('Options', () => {
  var Options = require('../../../src/plugin/options.js')

  it('override options', () => {
    var options = new Options({
      'enabled': false,
      'app.https': true,
      'host': 'a',
      'accountCode': 'b',
      'user.name': 'c',
      'obfuscateIp': false,
      'userType': 'ad',
      'referer': 'bc',
      'experiments': ['bd', 'be', 'bf'],
      'parse.hls': true,
      'parse.cdnNameHeader': 'd',
      'parse.cdnNode': true,
      'parse.cdnNode.list': ['e'],
      'parse.locationHeader': false,
      'network.ip': 'f',
      'network.isp': 'g',
      'network.connectionType': 'h',
      'device.code': 'i',
      'content.resource': 'j',
      'content.isLive': true,
      'content.title': 'k',
      'content.title2': 'l',
      'content.duration': 1,
      'content.transactionCode': 'm',
      'content.bitrate': 2,
      'content.throughput': 3,
      'content.rendition': 'n',
      'content.cdn': 'o',
      'content.cdnNode': 'ae',
      'content.fps': 'af',
      'content.cdnType': 1,
      'content.streamingProtocol': 'HLS',
      'content.metadata': { p: 'q' },
      'ad.metadata': { r: 's' },
      'ad.resource': 'ag',
      'ad.title': 'ah',
      'ad.ignore': false,
      'ad.afterStop': false,
      'smartswitch.configCode': 'code1',
      'smartswitch.groupCode': 'code2',
      'smartswitch.contractCode': 'code3',
      'content.customDimension.1': '2t',
      'content.customDimension.2': '2u',
      'content.customDimension.3': '2v',
      'content.customDimension.4': '2w',
      'content.customDimension.5': '2x',
      'content.customDimension.6': '2y',
      'content.customDimension.7': '2z',
      'content.customDimension.8': '2aa',
      'content.customDimension.9': '2ab',
      'content.customDimension.10': '2ac',
      'content.customDimension.11': '2ai',
      'content.customDimension.12': '2aj',
      'content.customDimension.13': '2ak',
      'content.customDimension.14': '2al',
      'content.customDimension.15': '2am',
      'content.customDimension.16': '2an',
      'content.customDimension.17': '2ao',
      'content.customDimension.18': '2ap',
      'content.customDimension.19': '2aq',
      'content.customDimension.20': '2ar',
      'ad.customDimension.1': '2as',
      'ad.customDimension.2': '2at',
      'ad.customDimension.3': '2au',
      'ad.customDimension.4': '2av',
      'ad.customDimension.5': '2aw',
      'ad.customDimension.6': '2ax',
      'ad.customDimension.7': '2ay',
      'ad.customDimension.8': '2az',
      'ad.customDimension.9': '2ba',
      'ad.customDimension.10': '2bb'
    })

    expect(options['enabled']).toBe(false)
    expect(options['app.https']).toBe(true)
    expect(options['host']).toBe('a')
    expect(options['accountCode']).toBe('b')
    expect(options['user.name']).toBe('c')
    expect(options['user.obfuscateIp']).toBe(false)
    expect(options['user.type']).toBe('ad')
    expect(options['referer']).toBe('bc')
    expect(options['experiments'][0]).toBe('bd')
    expect(options['experiments'][1]).toBe('be')
    expect(options['experiments'][2]).toBe('bf')
    expect(options['experiments'][3]).not.toBeDefined()
    expect(options['parse.hls']).toBe(true)
    expect(options['parse.cdnNameHeader']).toBe('d')
    expect(options['parse.cdnNode']).toBe(true)
    expect(options['parse.cdnNode.list'][0]).toBe('e')
    expect(options['parse.locationHeader']).toBe(false)
    expect(options['network.ip']).toBe('f')
    expect(options['network.isp']).toBe('g')
    expect(options['network.connectionType']).toBe('h')
    expect(options['device.code']).toBe('i')
    expect(options['content.resource']).toBe('j')
    expect(options['content.isLive']).toBe(true)
    expect(options['content.title']).toBe('k')
    expect(options['content.program']).toBe('l')
    expect(options['content.duration']).toBe(1)
    expect(options['content.transactionCode']).toBe('m')
    expect(options['content.bitrate']).toBe(2)
    expect(options['content.throughput']).toBe(3)
    expect(options['content.rendition']).toBe('n')
    expect(options['content.cdn']).toBe('o')
    expect(options['content.metadata'].p).toBe('q')
    expect(options['content.cdnNode']).toBe('ae')
    expect(options['content.fps']).toBe('af')
    expect(options['content.cdnType']).toBe(1)
    expect(options['content.streamingProtocol']).toBe('HLS')
    expect(options['ad.metadata'].r).toBe('s')
    expect(options['ad.resource']).toBe('ag')
    expect(options['ad.title']).toBe('ah')
    expect(options['ad.ignore']).toBe(false)
    expect(options['ad.afterStop']).toBe(false)
    expect(options['smartswitch.configCode']).toBe('code1')
    expect(options['smartswitch.groupCode']).toBe('code2')
    expect(options['smartswitch.contractCode']).toBe('code3')

    expect(options['content.customDimension.1']).toBe('2t')
    expect(options['content.customDimension.2']).toBe('2u')
    expect(options['content.customDimension.3']).toBe('2v')
    expect(options['content.customDimension.4']).toBe('2w')
    expect(options['content.customDimension.5']).toBe('2x')
    expect(options['content.customDimension.6']).toBe('2y')
    expect(options['content.customDimension.7']).toBe('2z')
    expect(options['content.customDimension.8']).toBe('2aa')
    expect(options['content.customDimension.9']).toBe('2ab')
    expect(options['content.customDimension.10']).toBe('2ac')
    expect(options['content.customDimension.11']).toBe('2ai')
    expect(options['content.customDimension.12']).toBe('2aj')
    expect(options['content.customDimension.13']).toBe('2ak')
    expect(options['content.customDimension.14']).toBe('2al')
    expect(options['content.customDimension.15']).toBe('2am')
    expect(options['content.customDimension.16']).toBe('2an')
    expect(options['content.customDimension.17']).toBe('2ao')
    expect(options['content.customDimension.18']).toBe('2ap')
    expect(options['content.customDimension.19']).toBe('2aq')
    expect(options['content.customDimension.20']).toBe('2ar')
    expect(options['ad.customDimension.1']).toBe('2as')
    expect(options['ad.customDimension.2']).toBe('2at')
    expect(options['ad.customDimension.3']).toBe('2au')
    expect(options['ad.customDimension.4']).toBe('2av')
    expect(options['ad.customDimension.5']).toBe('2aw')
    expect(options['ad.customDimension.6']).toBe('2ax')
    expect(options['ad.customDimension.7']).toBe('2ay')
    expect(options['ad.customDimension.8']).toBe('2az')
    expect(options['ad.customDimension.9']).toBe('2ba')
    expect(options['ad.customDimension.10']).toBe('2bb')
  })

  it('not to throw if a non-object is passed', () => {
    var options = new Options()
    expect(function () { options.setOptions(1) }).not.toThrow()
  })

  it('not to throw if a non-object is passed as base', () => {
    var options = new Options()
    expect(function () { options.setOptions({ 'parse.cdnNameHeader': 'd' }, 1) }).not.toThrow()
  })

  it('should replace extraparams and adextraparams', () => {
    var options = new Options({
      'content.customDimension.1': 'test1',
      'content.customDimension.3': 'test3',
      'ad.customDimension.1': 'adtest1',
      'ad.customDimension.3': 'adtest3'
    })
    options.setCustomDimensions(['a', 'b'])
    options.setAdCustomDimensions(['c', 'd'])
    expect(options['content.customDimension.1']).toBe('a')
    expect(options['content.customDimension.2']).toBe('b')
    expect(options['content.customDimension.3']).toBeNull()
    expect(options['ad.customDimension.1']).toBe('c')
    expect(options['ad.customDimension.2']).toBe('d')
    expect(options['ad.customDimension.3']).toBeNull()
  })
})
