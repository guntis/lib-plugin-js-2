describe('Device detector', () => {
  var BackgroundDetector = require('../../../src/detectors/backgroundDetector')

  it('should be created', () => {
    var pluginRef = {
      options: {
        'background.settings': null
      }
    }
    var detector = new BackgroundDetector(pluginRef)
    expect(detector.plugin).toBe(pluginRef)
    expect(detector.isInBackground).toBeFalsy()
    expect(detector.isBackgroundDetectorStarted).toBeFalsy()
  })

  it('should detect going to background', () => {
    var detector = new BackgroundDetector()
    spyOn(detector, '_getSettings')
    detector.startDetection()
    detector._visibilityListener() // trigger window.document.visibilitychange
    expect(detector._getSettings).toHaveBeenCalled()
  })

  it('should detect going to foreground', () => {
    var detector = new BackgroundDetector()
    spyOn(detector, '_getSettings')
    detector.startDetection()
    window.document.visibilityState = 'visible'
    detector._visibilityListener() // trigger window.document.visibilitychange
    expect(detector._getSettings).toHaveBeenCalled()
  })

  it('should get the right settings with generic', () => {
    var detector = new BackgroundDetector()
    detector.plugin = {
      deviceDetector: {
        isDesktop: function () { return false },
        isAndroid: function () { return true },
        isIphone: function () { return false },
        isSmartTV: function () { return false }
      },
      options: {
        'background.settings': 'pause',
        'background.settings.android': 'stop'
      }
    }
    expect(detector._getSettings()).toBe('pause')
  })

  it('should get the right settings with no generic, android', () => {
    var detector = new BackgroundDetector()
    detector.plugin = {
      deviceDetector: {
        isDesktop: function () { return false },
        isAndroid: function () { return true },
        isIphone: function () { return false },
        isSmartTV: function () { return false }
      },
      options: {
        'background.settings.android': 'pause'
      }
    }
    expect(detector._getSettings()).toBe('pause')
  })

  it('should get the right settings with no generic, iphone', () => {
    var detector = new BackgroundDetector()
    detector.plugin = {
      deviceDetector: {
        isDesktop: function () { return false },
        isAndroid: function () { return false },
        isIphone: function () { return true },
        isSmartTV: function () { return false }
      },
      options: {
        'background.settings.iOS': 'pause'
      }
    }
    expect(detector._getSettings()).toBe('pause')
  })

  it('should get the right settings with no generic, desktop', () => {
    var detector = new BackgroundDetector()
    detector.plugin = {
      deviceDetector: {
        isDesktop: function () { return true },
        isAndroid: function () { return false },
        isIphone: function () { return false },
        isSmartTV: function () { return false }
      },
      options: {
        'background.settings.desktop': 'pause'
      }
    }
    expect(detector._getSettings()).toBe('pause')
  })

  it('should get the right settings with no generic, smartTv', () => {
    var detector = new BackgroundDetector()
    detector.plugin = {
      deviceDetector: {
        isDesktop: function () { return false },
        isAndroid: function () { return true },
        isIphone: function () { return false },
        isSmartTV: function () { return true }
      },
      options: {
        'background.settings.tv': 'pause'
      }
    }
    expect(detector._getSettings()).toBe('pause')
  })

  it('should call expected methods for firepause', () => {
    var detector = new BackgroundDetector()
    var adapter = {
      flags: {
        isStarted: true,
        isEnded: false
      },
      fireStop: function () { return true },
      firePause: function () { return true }
    }
    var adsAdapter = {
      flags: {
        isStarted: true,
        isEnded: false
      },
      fireStop: function () { return true },
      firePause: function () { return true }
    }
    detector.plugin = {
      '_adapter': adapter,
      '_adsAdapter': adsAdapter,
      getAdapter: function () { return this._adapter },
      getAdsAdapter: function () { return this._adsAdapter }
    }
    spyOn(detector.plugin._adapter, 'firePause')
    spyOn(detector.plugin._adsAdapter, 'firePause')
    detector._firePause()
    expect(detector.plugin._adapter.firePause).not.toHaveBeenCalled()
    expect(detector.plugin._adsAdapter.firePause).toHaveBeenCalled()
    detector.plugin._adsAdapter.flags.isStarted = false
    detector._firePause()
    expect(detector.plugin._adapter.firePause).toHaveBeenCalled()
  })

  it('should call plugin stop', () => {
    var detector = new BackgroundDetector()
    detector.plugin = {
      fireStop: function () { },
      getAdapter: function () { return null },
      getAdsAdapter: function () { return null }
    }
    spyOn(detector.plugin, 'fireStop')
    detector._fireStop()
    expect(detector.plugin.fireStop).toHaveBeenCalled()
  })

  it('should call plugin pause, ads not playing', () => {
    var detector = new BackgroundDetector()
    var adapter1 = {
      flags: {
        isStarted: true
      },
      firePause: function () { }
    }
    var adapter2 = {
      flags: {
        isStarted: false
      },
      firePause: function () { }
    }
    detector.plugin = {
      fireStop: function () { },
      getAdapter: function () { return adapter1 },
      getAdsAdapter: function () { return adapter2 }
    }
    spyOn(adapter1, 'firePause')
    spyOn(adapter2, 'firePause')
    detector._firePause()
    expect(adapter1.firePause).toHaveBeenCalled()
    expect(adapter2.firePause).not.toHaveBeenCalled()
  })

  it('should call plugin pause, ads playing', () => {
    var detector = new BackgroundDetector()
    var adapter1 = {
      flags: {
        isStarted: false
      },
      firePause: function () { }
    }
    var adapter2 = {
      flags: {
        isStarted: true
      },
      firePause: function () { }
    }
    detector.plugin = {
      fireStop: function () { },
      getAdapter: function () { return adapter1 },
      getAdsAdapter: function () { return adapter2 }
    }
    spyOn(adapter1, 'firePause')
    spyOn(adapter2, 'firePause')
    detector._firePause()
    expect(adapter1.firePause).not.toHaveBeenCalled()
    expect(adapter2.firePause).toHaveBeenCalled()
  })

  it('should call expected methods for firestop', () => {
    var detector = new BackgroundDetector()
    var adapter = {
      flags: {
        isStarted: true,
        isEnded: false
      },
      fireStop: function () { return true },
      firePause: function () { return true }
    }
    var adsAdapter = {
      flags: {
        isStarted: true,
        isEnded: false
      },
      fireStop: function () { return true },
      firePause: function () { return true }
    }
    detector.plugin = {
      '_adapter': adapter,
      '_adsAdapter': adsAdapter,
      getAdapter: function () { return this._adapter },
      getAdsAdapter: function () { return this._adsAdapter },
      fireStop: function () { return null }
    }
    spyOn(detector.plugin._adapter, 'fireStop')
    spyOn(detector.plugin._adsAdapter, 'fireStop')
    spyOn(detector.plugin, 'fireStop')
    detector._fireStop()
    expect(detector.plugin._adapter.fireStop).not.toHaveBeenCalled()
    expect(detector.plugin._adsAdapter.fireStop).toHaveBeenCalled()
    expect(detector.plugin.fireStop).toHaveBeenCalled()
    detector.plugin._adapter = null
    detector._fireStop()
    expect(detector.plugin.fireStop).toHaveBeenCalled()
  })
})
