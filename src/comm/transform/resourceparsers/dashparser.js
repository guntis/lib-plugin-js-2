var YBRequest = require('../../request')
var Emitter = require('../../../emitter')
var Log = require('../../../log')

var DashParser = Emitter.extend(
  /** @lends youbora.DashParser.prototype */
  {
    /**
         * Class that asynchronously parses an DASH resource in order to get to the location URL.
         *
         * Since the CDN detection is performed with the resource url, it is essential that this
         * resource url is pointing to the CDN that is actually hosting the manifest.
         *
         * @constructs DashParser
         * @extends youbora.Emitter
         * @memberof youbora
         */
    constructor: function () {
      this._realResource = null
    },

    /**
         * Emits DONE event
         */
    done: function () {
      this.emit(DashParser.Event.DONE)
    },

    /**
         * Starts the Dash parsing from the given resource. The first (outside) call should set the
         * parentResource to null.
         *
         * @param {string} resource The resource url.
         */
    parse: function (resource) {
      var request = new YBRequest(resource, null, null, { cache: true })

      request.on(YBRequest.Event.SUCCESS, function (resp) {
        var manifest = resp.getXHR().responseText
        var begin = manifest.indexOf('<Location>')
        var end = manifest.indexOf('</Location>')
        if (begin > -1 && end > -1) {
          this._realResource = manifest.slice(begin + 10, end)
        } else {
          Log.warn('The Dash manifest doesnt contain Location tag')
        }
        this.done()
      }.bind(this))

      request.on(YBRequest.Event.ERROR, function (resp) {
        this.done()
      }.bind(this))

      request.send()
    },

    /**
         * Get the parsed resource. Will be null/undefined if parsing is not yet started and can be a partial
         * (an intermediate manifest) result if the parser is still running.
         *
         * @return {string} The parsed resource.
         */
    getResource: function () {
      return this._realResource
    }
  },

  /** @lends youbora.DashParser */
  {
    // Static members

    /**
         * List of events that could be fired from this class.
         * @enum
         */
    Event: {
      /** Notifies that this DashParser is done processing. */
      DONE: 'done'
    }
  }
)

module.exports = DashParser
