// {@see CdnParser}

module.exports = {
  parsers: [{
    element: 'name',
    headerName: null,
    regex: /(.+)/
  }]
}
