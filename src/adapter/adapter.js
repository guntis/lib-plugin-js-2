var Emitter = require('../emitter')
var Log = require('../log')
var Util = require('../util')
var version = require('../version')
var PlaybackChronos = require('./playbackchronos')
var PlaybackFlags = require('./playbackflags')
var PlayheadMonitor = require('./playheadmonitor')
var Chrono = require('../chrono')

var Adapter = Emitter.extend(
  /** @lends youbora.Adapter.prototype */
  {
    /**
     * Main Adapter class. All specific player adapters should extend this class specifying a player
     * class.
     *
     * The Adapter works as the 'glue' between the player and YOUBORA acting both as event
     * translator and as proxy for the {@link Plugin} to get info from the player.
     *
     * @constructs Adapter
     * @extends youbora.Emitter
     * @memberof youbora
     *
     * @param {object|string} player Either an instance of the player or a string containing an ID.
     */
    constructor: function (player) {
      /** An instance of {@link FlagStatus} */
      this.flags = new PlaybackFlags()

      /** An instance of {@link ChronoStatus} */
      this.chronos = new PlaybackChronos()

      /** Reference to {@link PlayheadMonitor}. Helps the plugin detect buffers/seeks. */
      this.monitor = null

      /** Reference to {@link Plugin}. */
      this.plugin = null

      /** Reference to the player tag */
      this.player = null

      // Register player and event listeners
      this.setPlayer(player)

      /** Reference to the video/object tag, could be the same as the player. */
      this.tag = this.player

      Log.notice('Adapter ' + this.getVersion() + ' with Lib ' + version + ' is ready.')
    },

    /**
     * Sets a new player, removes the old listeners if needed.
     *
     * @param {Object} player Player to be registered.
     */
    setPlayer: function (player) {
      this.unregisterListeners()

      if (typeof player === 'string' && typeof document !== 'undefined') {
        this.player = document.getElementById(player)
      } else {
        this.player = player
      }
      this.registerListeners()
    },

    /**
     * Override to create event binders.
     * It's a good practice when implementing a new Adapter to create intermediate methods and call
     * those when player events are detected instead of just calling the `fire*` methods. This
     * will allow future users of the Adapter to customize its behaviour by overriding these
     * methods.
     *
     * @example
     * registerListeners: function () {
     *  this.player.addEventListener('start', this.onStart.bind(this))
     * },
     *
     * onStart: function (e) {
     *  this.emit('start')
     * }
     */
    registerListeners: function () {
    },

    /**
     * Override to create event de-binders.
     *
     * @example
     * registerListeners: function () {
     *  this.player.removeEventListener('start', this.onStart)
     * }
     */
    /** Unregister listeners to this.player. */
    unregisterListeners: function () {
    },

    /**
     * This function disposes the currend adapter, removes player listeners and drops references.
     */
    dispose: function () {
      if (this.monitor) this.monitor.stop()
      this.fireStop()
      this.unregisterListeners()
      this.player = null
      this.tag = null
    },

    /**
     * Creates a new {@link PlayheadMonitor} at this.monitor.
     *
     * @param {bool} monitorBuffers If true, it will monitor buffers.
     * @param {bool} monitorSeeks If true, it will monitor seeks.
     * @param {number} [interval=800] The interval time in ms.
     */
    monitorPlayhead: function (monitorBuffers, monitorSeeks, interval) {
      var type = 0
      if (monitorBuffers) type |= PlayheadMonitor.Type.BUFFER
      if (monitorSeeks) type |= PlayheadMonitor.Type.SEEK

      this.monitor = new PlayheadMonitor(this, type, interval)
    },

    stopMonitor: function () {
      if (this.monitor) this.monitor.stop()
    },

    // GETTERS //

    /** Override to return current playhead of the video */
    getPlayhead: function () {
      return null
    },

    /** Override to return current playrate */
    getPlayrate: function () {
      return !this.flags.isPaused ? 1 : 0
    },

    /** Override to return Frames Per Secon (FPS) */
    getFramesPerSecond: function () {
      return null
    },

    /** Override to return dropped frames since start */
    getDroppedFrames: function () {
      return null
    },

    /** Override to return video duration */
    getDuration: function () {
      return null
    },

    /** Override to return current bitrate */
    getBitrate: function () {
      return null
    },

    /** Override to return user bandwidth throughput */
    getThroughput: function () {
      return null
    },

    /** Override to return rendition */
    getRendition: function () {
      return null
    },

    /** Override to return title */
    getTitle: function () {
      return null
    },

    /** Override to return title2 */
    getTitle2: function () {
      return null
    },

    /** Override to recurn true if live and false if VOD */
    getIsLive: function () {
      return null
    },

    /** Override to return resource URL. */
    getResource: function () {
      return null
    },

    /** Override to return player version */
    getPlayerVersion: function () {
      return null
    },

    /** Override to return player's name */
    getPlayerName: function () {
      return null
    },

    /** Override to return adapter version. */
    getVersion: function () {
      return version + '-generic-js'
    },

    /** Override to return CDN traffic bytes not using streamroot or peer5. */
    getCdnTraffic: function () {
      return null
    },

    /** Override to return P2P traffic bytes not using streamroot or peer5. */
    getP2PTraffic: function () {
      return null
    },

    /** Override to return P2P traffic sent in bytes, not using streamroot or peer5. */
    getUploadTraffic: function () {
      return null
    },

    /** Override to return if P2P is enabled not using streamroot or peer5. */
    getIsP2PEnabled: function () {
      return null
    },
    /** Override to return household id */
    getHouseholdId: function () {
      return null
    },

    /** Override to return the latency */
    getLatency: function () {
      return null
    },

    /** Override to return the number of packets lost */
    getPacketLoss: function () {
      return null
    },

    /** Override to return the number of packets sent */
    getPacketSent: function () {
      return null
    },

    /** Override to return a json with metrics */
    getMetrics: function () {
      return null
    },

    // Only ads

    /** Override to return current ad position (only ads) */
    getPosition: function () {
      return null
    },

    /** Override to return the given ad structure (list with number of pre, mid, and post breaks) (only ads) */
    getGivenBreaks: function () {
      return null
    },

    /** Override to return the ad structure requested (list with number of pre, mid, and post breaks) (only ads) */
    getExpectedBreaks: function () {
      return null
    },

    /** Override to return the structure of ads requested (only ads) */
    getExpectedPattern: function () {
      return null
    },

    /** Override to return a list of playheads of ad breaks begin time (only ads) */
    getBreaksTime: function () {
      return null
    },

    /** Override to return the number of ads given for the break (only ads) */
    getGivenAds: function () {
      return null
    },

    /** Override to return the number of ads requested for the break (only ads) */
    getExpectedAds: function () {
      return null
    },

    /** Override to return if the ad is being shown in the screen or not
     * The standard definition is: more than 50% of the pixels of the ad are on the screen
     * (only ads)
     */
    getIsVisible: function () {
      return true
    },

    /** Override to return a boolean showing if the audio is enabled when the ad begins (only ads) */
    getAudioEnabled: function () {
      return null
    },

    /** Override to return if the ad is skippable (only ads) */
    getIsSkippable: function () {
      return null
    },

    /** Override to return a boolean showing if the player is in fullscreen mode when the ad begins (only ads) */
    getIsFullscreen: function () {
      return null
    },

    /** Override to return the ad campaign (only ads) */
    getCampaign: function () {
      return null
    },

    /** Override to return the ad creative id (only ads) */
    getCreativeId: function () {
      return null
    },

    /** Override to return the ad provider name (only ads) */
    getProvider: function () {
      return null
    },

    // FLOW //

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent init if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireInit: function (params) {
      if (this.plugin) this.plugin.fireInit()
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireStart: function (params) {
      if (this.plugin && this.plugin.backgroundDetector && this.plugin.backgroundDetector.canBlockStartCalls()) {
        return null
      }
      if (!this.flags.isStarted) {
        this.flags.isStarted = true
        this.chronos.total.start()
        this.chronos.join.start()
        this.emit(Adapter.Event.START, { params: params })
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireJoin: function (params) {
      if (!this.flags.isJoined && !this.flags.isStarted && !this._isAds() && this.plugin && this.plugin.isInitiated) {
        this.fireStart()
      }
      if (this.flags.isStarted && !this.flags.isJoined) {
        this.flags.isStarted = true
        if (this.monitor) this.monitor.start()
        this.flags.isJoined = true
        this.chronos.join.stop()
        this.emit(Adapter.Event.JOIN, { params: params })
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    firePause: function (params) {
      if (this.flags.isBuffering) {
        this.fireBufferEnd()
      }
      if (this.flags.isJoined && !this.flags.isPaused) {
        this.flags.isPaused = true

        this.chronos.pause.start()

        this.emit(Adapter.Event.PAUSE, { params: params })
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireResume: function (params) {
      if (this.flags.isJoined && this.flags.isPaused) {
        this.flags.isPaused = false

        this.chronos.pause.stop()

        if (this.monitor) this.monitor.skipNextTick()

        this.emit(Adapter.Event.RESUME, { params: params })
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     * @param {bool} [convertFromSeek=false] If true, will convert current seek to buffer.
     */
    fireBufferBegin: function (params, convertFromSeek) {
      if (this.flags.isJoined && !this.flags.isBuffering) {
        if (this.flags.isSeeking) {
          if (convertFromSeek) {
            Log.notice('Converting current buffer to seek')

            this.chronos.buffer = this.chronos.seek.clone()
            this.chronos.seek.reset()

            this.flags.isSeeking = false
          } else {
            return
          }
        } else {
          this.chronos.buffer.start()
        }

        this.flags.isBuffering = true
        this.emit(Adapter.Event.BUFFER_BEGIN, { params: params })
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireBufferEnd: function (params) {
      if (this.flags.isJoined && this.flags.isBuffering) {
        this.flags.isBuffering = false

        this.chronos.buffer.stop()

        if (this.monitor) this.monitor.skipNextTick()

        this.emit(Adapter.Event.BUFFER_END, { params: params })
      }
    },

    /**
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    cancelBuffer: function (params) {
      if (this.flags.isJoined && this.flags.isBuffering) {
        this.flags.isBuffering = false

        this.chronos.buffer.stop()

        if (this.monitor) this.monitor.skipNextTick()
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     * @param {bool} [convertFromBuffer=true] If false, will convert current buffer to seek.
     */
    fireSeekBegin: function (params, convertFromBuffer) {
      if (this.plugin && this.plugin.getIsLive() && this.plugin.options['content.isLive.noSeek']) return null
      if (this.flags.isJoined && !this.flags.isSeeking) {
        if (this.flags.isBuffering) {
          if (convertFromBuffer !== false) {
            Log.notice('Converting current buffer to seek')

            this.chronos.seek = this.chronos.buffer.clone()
            this.chronos.buffer.reset()

            this.flags.isBuffering = false
          } else {
            return
          }
        } else {
          this.chronos.seek.start()
        }

        this.flags.isSeeking = true
        this.emit(Adapter.Event.SEEK_BEGIN, { params: params })
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireSeekEnd: function (params) {
      if (this.plugin && this.plugin.getIsLive() && this.plugin.options['content.isLive.noSeek']) return null
      if (this.flags.isJoined && this.flags.isSeeking) {
        this.flags.isSeeking = false

        this.chronos.seek.stop()

        if (this.monitor) this.monitor.skipNextTick()

        this.emit(Adapter.Event.SEEK_END, { params: params })
      }
    },

    /**
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    cancelSeek: function (params) {
      if (this.flags.isJoined && this.flags.isSeeking) {
        this.flags.isSeeking = false

        this.chronos.seek.stop()

        if (this.monitor) this.monitor.skipNextTick()
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireStop: function (params) {
      if (this._isAds() || (this.plugin && this.plugin._isStopReady())) {
        if ((this._isAds() && this.flags.isStarted) ||
          (!this._isAds() && (this.flags.isStarted || (this.plugin && this.plugin.isInitiated)))
        ) {
          if (this.monitor) this.monitor.stop()

          this.flags.reset()
          this.chronos.total.stop()
          this.chronos.join.reset()
          this.chronos.pause.stop()
          this.chronos.buffer.stop()
          this.chronos.seek.stop()

          this.emit(Adapter.Event.STOP, { params: params })

          this.chronos.pause.reset()
          this.chronos.buffer.reset()
          this.chronos.seek.reset()
          this.chronos.viewedMax.splice(0, this.chronos.viewedMax.length)
        }
      }
    },

    _isAds: function () {
      return !!this.getPosition()
    },

    /**
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireCasted: function (params) {
      if (!params) params = {}
      params.casted = true
      this.fireStop(params)
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {String|Object} [code] Error Code, if an object is sent, it will be treated as params.
     * @param {String} [msg] Error Message
     * @param {Object} [metadata] Object defining error metadata
     * @param {String} [level] Level of the error. Currently supports 'error' and 'fatal'
     */
    fireError: function (code, msg, metadata, level) {
      var params = Util.buildErrorParams(code, msg, metadata, level)
      if (params.code) {
        delete params.code
      }
      this.emit(Adapter.Event.ERROR, { params: params })
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {String|Object} [code] Error Code, if an object is sent, it will be treated as params.
     * @param {String} [msg] Error Message
     * @param {Object} [metadata] Object defining error metadata
     */
    fireFatalError: function (code, msg, metadata, level) {
      if (this.monitor) this.monitor.stop()
      this.fireError(code, msg, metadata, level)
      this.fireStop()
    },

    /**
     * ONLY ADS.
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireClick: function (params) {
      if (typeof params === 'string') {
        params = { 'url': params }
      }
      this.emit(Adapter.Event.CLICK, { params: params })
    },

    /**
     * ONLY ADS.
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireQuartile: function (quartile) {
      if (!this.flags.isStarted || !quartile || quartile < 1 || quartile > 3) return
      this.emit(Adapter.Event.QUARTILE, { params: { 'quartile': quartile } })
    },

    /**
    * ONLY ADS.
    * Starts a new chrono for viewedMax and restarts viewedAcum
    */
    startChronoView: function () {
      if (this.getIsVisible() && !this.plugin.backgroundDetector.isInBackground) {
        // If is not stopped
        if (this.chronos.viewedMax.length === 0 || this.chronos.viewedMax[this.chronos.viewedMax.length - 1].stopTime !== 0) {
          this.chronos.viewedMax.push(new Chrono())
          this.chronos.viewedMax[this.chronos.viewedMax.length - 1].start()
        }
      }
    },

    /**
    * ONLY ADS.
    * Stops a new chrono for viewedMax and restarts viewedAcum
    */
    stopChronoView: function () {
      if (this.chronos.viewedMax[0]) {
        // if not stopped
        if (this.chronos.viewedMax.length > 0 && this.chronos.viewedMax[this.chronos.viewedMax.length - 1].stopTime === 0) {
          this.chronos.viewedMax[this.chronos.viewedMax.length - 1].stop()
        }
      }
    },

    /**
     * ONLY ADS.
    * @param {Object} [params] Object of key:value params to add to the request.
    */
    fireManifest: function (params, message) {
      if (typeof params === 'string') {
        this.emit(Adapter.Event.MANIFEST, { 'params': { errorType: params, errorMessage: message } })
      } else {
        this.emit(Adapter.Event.MANIFEST, { params: params })
      }
    },

    /*
    * ONLY ADS.
    * Emits related event and set flags if current status is valid.
    * ie: won't sent start if isStarted is already true.
    *
    * @param {Object} [params] Object of key:value params to add to the request.
    */
    fireSkip: function (params) {
      if (params === undefined) {
        params = {}
      }
      params.skipped = true
      this.fireStop(params)
    },

    /**
    * ONLY ADS.
    * Emits related event and set flags if current status is valid.
    * ie: won't sent start if isStarted is already true.
    *
    * @param {Object} [params] Object of key:value params to add to the request.
    */
    fireBreakStart: function (params) {
      if (!this.plugin.isBreakStarted) {
        this.plugin.isBreakStarted = true
        this.emit(Adapter.Event.PODSTART, { params: params })
      }
    },

    /**
    * ONLY ADS.
    * Emits related event and set flags if current status is valid.
    * ie: won't sent start if isStarted is already true.
    *
    * @param {Object} [params] Object of key:value params to add to the request.
    */
    fireBreakStop: function (params) {
      if (this.plugin.isBreakStarted) {
        this.emit(Adapter.Event.PODSTOP, { params: params })
        this.plugin.isBreakStarted = false
      }
    },

    /**
     * Emits event request.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireEvent: function (eventName, dimensions, values) {
      var returnparams = {}
      returnparams.name = eventName || ''
      returnparams.dimensions = dimensions || {}
      returnparams.values = values || {}
      this.emit(Adapter.Event.VIDEO_EVENT, { 'params': returnparams })
    }
  },
  {
    /** @lends youbora.Adapter */
    // Static Memebers //

    /**
     * List of events that could be fired
     * @enum
     * @event
     */
    Event: {
      START: 'start',
      JOIN: 'join',
      PAUSE: 'pause',
      RESUME: 'resume',
      SEEK_BEGIN: 'seek-begin',
      SEEK_END: 'seek-end',
      BUFFER_BEGIN: 'buffer-begin',
      BUFFER_END: 'buffer-end',
      ERROR: 'error',
      STOP: 'stop',
      CLICK: 'click',
      MANIFEST: 'manifest',
      PODSTART: 'break-start',
      PODSTOP: 'break-stop',
      QUARTILE: 'quartile',
      VIDEO_EVENT: 'video-event'
    }
  }
)

module.exports = Adapter
