var Log = require('../log')
var Constants = require('../constants')

// This file is designed to add extra functionalities to Plugin class

var PluginPingMixin = {
  /**
   * Starts sending pings.
   *
   * @private
   * @memberof youbora.Plugin.prototype
   */
  _startPings: function () {
    if (!this._ping.isRunning) this._ping.start()
  },

  /**
   * Stops sending pings.
   *
   * @private
   * @memberof youbora.Plugin.prototype
   */
  _stopPings: function () {
    this._ping.stop()
  },

  /**
   * Sends ping request
   *
   * @param {number} diffTime Time since the last ping
   *
   * @private
   * @memberof youbora.Plugin.prototype
   */
  _sendPing: function (diffTime) {
    var params = {
      diffTime: diffTime,
      entities: this.requestBuilder.getChangedEntities()
    }
    if (this._adapter) {
      if (this._adapter.flags.isPaused) {
        params = this.requestBuilder.fetchParams(params, ['pauseDuration'])
      } else {
        params = this.requestBuilder.fetchParams(params, ['bitrate', 'throughput', 'fps'])
      }
      if (this._adapter.flags.isJoined) {
        params = this.requestBuilder.fetchParams(params, ['playhead'])
      }
      if (this._adapter.flags.isBuffering) {
        params = this.requestBuilder.fetchParams(params, ['bufferDuration'])
      }
      if (this._adapter.flags.isSeeking) {
        params = this.requestBuilder.fetchParams(params, ['seekDuration'])
      }

      if (this._adsAdapter && !this.options['ad.ignore']) {
        if (this._adsAdapter.flags.isStarted) {
          params = this.requestBuilder.fetchParams(params, ['adPlayhead', 'adViewedDuration', 'adViewability'])
          if (this._adsAdapter.flags.isPaused) {
            params = this.requestBuilder.fetchParams(params, ['adPauseDuration'])
          } else {
            params = this.requestBuilder.fetchParams(params, ['adBitrate'])
          }
        }
        if (this._adsAdapter.flags.isBuffering) {
          params = this.requestBuilder.fetchParams(params, ['adBufferDuration'])
        }
      }
    }

    this._send(Constants.WillSendEvent.WILL_SEND_PING, Constants.Service.PING, params)
    if (this.startDelayed) {
      this._retryStart()
    }
    Log.verbose(Constants.Service.PING)
  }
}

module.exports = PluginPingMixin
