// This file is designed to add extra functionalities to Plugin class

var PluginInfinityGettersMixin = {
  getContext: function () {
    if (this.options['session.context']) {
      return this.storage.getSession('context')
    } else {
      return 'Default'
    }
  },

  getSessionId: function () {
    return this.storage.getStorages('session')
  },

  getStoredData: function () {
    return this.storage.getStorages('data')
  },

  getDataTime: function () {
    return this.storage.getStorages('dataTime')
  },

  getLastActive: function () {
    return this.storage.getStorages('lastactive')
  },

  setStoredData: function (data) {
    this.storage.setStorages('data', data)
  },

  setSessionId: function (session) {
    this.storage.setStorages('session', session)
  },

  setDataTime: function (time) {
    this.storage.setStorages('dataTime', time)
  },

  setLastActive: function (last) {
    this.storage.setStorages('lastactive', last)
  },

  getPageName: function () {
    if (typeof document !== 'undefined' && document.title) {
      return document.title
    }
  },

  getIsSessionExpired: function () {
    var now = new Date().getTime()
    return !this.getSessionId() || (this.infinity.getFirstActive() < now - this.sessionExpire)
  },

  getIsDataExpired: function () {
    var now = new Date().getTime()
    if (!this.storage.isEnabled()) return true
    return !this.getStoredData() || (this.infinity.getFirstActive() < now - this.sessionExpire)
  }
}

module.exports = PluginInfinityGettersMixin
