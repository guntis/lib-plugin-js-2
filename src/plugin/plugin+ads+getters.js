var Log = require('../log')
var Util = require('../util')
var Constants = require('../constants')

// This file is designed to add extra functionalities to Plugin class

var PluginAdsGettersMixin = {
  /**
   * Returns ads's PlayerVersion
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdPlayerVersion: function () {
    var ret = ''
    if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getPlayerVersion()
      } catch (err) {
        Log.warn('An error occured while calling getAdPlayerVersion', err)
      }
    }
    return ret
  },

  /**
   * Returns ad's position
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdPosition: function () {
    var ret = Constants.AdPosition.Preroll
    if (this._adsAdapter) {
      try {
        var temporalRet = this._adsAdapter.getPosition()
        if (Constants.AdPosition.Preroll === temporalRet ||
          Constants.AdPosition.Midroll === temporalRet ||
          Constants.AdPosition.Postroll === temporalRet) {
          ret = temporalRet
        }
      } catch (err) {
        Log.warn('An error occured while calling getAdPosition', err)
      }
    }
    if (!ret && this._adapter) {
      ret = (this._adapter.flags.isJoined) ? Constants.AdPosition.Midroll : Constants.AdPosition.Preroll
    }
    return ret
  },

  /**
   * Returns ad's AdPlayhead
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdPlayhead: function () {
    var ret = null
    if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getPlayhead()
      } catch (err) {
        Log.warn('An error occured while calling getAdPlayhead', err)
      }
    }
    return Util.parseNumber(ret, 0)
  },

  /**
   * Returns ad's AdDuration
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdDuration: function () {
    var ret = null
    if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getDuration()
      } catch (err) {
        Log.warn('An error occured while calling getAdDuration', err)
      }
    }
    return Util.parseNumber(ret, 0)
  },

  /**
   * Returns ad's AdBitrate
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdBitrate: function () {
    var ret = null
    if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getBitrate()
      } catch (err) {
        Log.warn('An error occured while calling getAdBitrate', err)
      }

      if (!ret || ret === -1) {
        ret = this.getWebkitAdBitrate()
      }
    }
    return Util.parseNumber(ret, -1)
  },

  /**
   * Returns bitrate as per webkitVideoDecodedByteCount
   *
   * @param {Object} tag Video tag DOM reference.
   * @returns {number}
   *
   * @memberof youbora.Plugin.prototype
   */
  getWebkitAdBitrate: function () {
    if (this._adapter && this._adapter.tag && this._adapter.tag.webkitVideoDecodedByteCount) {
      var bitrate = this._adapter.tag.webkitVideoDecodedByteCount
      if (this._lastWebkitAdBitrate) {
        var delta = this._adapter.tag.webkitVideoDecodedByteCount - this._lastWebkitAdBitrate
        bitrate = Math.round(((delta) / this.viewTransform.response.pingTime) * 8)
      }
      this._lastWebkitAdBitrate = this._adapter.tag.webkitVideoDecodedByteCount
      return bitrate !== 0 ? bitrate : -1
    }
  },

  /**
   * Returns ad's AdTitle
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdTitle: function () {
    var ret = null
    if (this.options['ad.title']) {
      return this.options['ad.title']
    }
    if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getTitle()
      } catch (err) {
        Log.warn('An error occured while calling getAdTitle', err)
      }
    }
    return ret
  },

  /**
   * Returns ad's AdResource
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdResource: function () {
    var ret = null
    if (this.options['ad.resource']) {
      return this.options['ad.resource']
    }
    if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getResource()
      } catch (err) {
        Log.warn('An error occured while calling getAdResource', err)
      }
    }
    return ret
  },

  /**
   * Returns ad's campaign
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdCampaign: function () {
    var ret = null
    if (this.options['ad.campaign']) {
      return this.options['ad.campaign']
    }
    if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getCampaign()
      } catch (err) {
        Log.warn('An error occured while calling getCampaign', err)
      }
    }
    return ret
  },

  /**
  * Returns ad's campaign
  *
  * @memberof youbora.Plugin.prototype
  */
  getAdCreativeId: function () {
    var ret = null
    if (this.options['ad.creativeId']) {
      return this.options['ad.creativeId']
    }
    if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getCreativeId()
      } catch (err) {
        Log.warn('An error occured while calling getCreativeId', err)
      }
    }
    return ret
  },

  /**
  * Returns ad's provider
  *
  * @memberof youbora.Plugin.prototype
  */
  getAdProvider: function () {
    var ret = null
    if (this.options['ad.provider']) {
      return this.options['ad.provider']
    }
    if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getProvider()
      } catch (err) {
        Log.warn('An error occured while calling getProvider', err)
      }
    }
    return ret
  },

  /**
   * Returns ads adapter getVersion or null
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdAdapterVersion: function () {
    var ret = null
    if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getVersion()
      } catch (err) {
        Log.warn('An error occured while calling getAdsAdapterVersion', err)
      }
    }
    return ret
  },

  /**
   * Returns ad's AdMetadata
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdMetadata: function () {
    return this.options['ad.metadata']
  },

  getGivenBreaks: function () {
    var ret = null
    if (this.options['ad.givenBreaks']) {
      ret = this.options['ad.givenBreaks']
    } else if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getGivenBreaks()
      } catch (err) {
        Log.warn('An error occured while calling getGivenBreaks', err)
      }
    }
    return ret
  },

  getExpectedBreaks: function () {
    var ret = null
    if (this.options['ad.expectedBreaks']) {
      ret = this.options['ad.expectedBreaks']
    } else if (this.options['ad.expectedPattern']) {
      ret = 0
      ret = this.options['ad.expectedPattern'].pre ? this.options['ad.expectedPattern'].pre.length : 0
      ret += this.options['ad.expectedPattern'].mid ? this.options['ad.expectedPattern'].mid.length : 0
      ret += this.options['ad.expectedPattern'].post ? this.options['ad.expectedPattern'].post.length : 0
    } else if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getExpectedBreaks()
      } catch (err) {
        Log.warn('An error occured while calling expectedBreaks', err)
      }
    }
    return ret
  },

  getExpectedPattern: function () {
    var ret = null
    if (this.options['ad.expectedPattern']) {
      ret = this.options['ad.expectedPattern']
    } else if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getExpectedPattern()
      } catch (err) {
        Log.warn('An error occured while calling expectedPattern', err)
      }
    }
    return ret
  },

  getBreaksTime: function () {
    var ret = null
    if (this.options['ad.breaksTime']) {
      ret = this.options['ad.breaksTime']
    } else if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getBreaksTime()
      } catch (err) {
        Log.warn('An error occured while calling breaksTime', err)
      }
    }
    return ret
  },

  getGivenAds: function () {
    var ret = null
    if (this.options['ad.givenAds']) {
      ret = this.options['ad.givenAds']
    } else if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getGivenAds()
      } catch (err) {
        Log.warn('An error occured while calling givenAds', err)
      }
    }
    return ret
  },

  getExpectedAds: function () {
    var ret = null
    try {
      if (this._adsAdapter) {
        if (this.options['ad.expectedPattern'] && this.getAdPosition()) {
          var list = []
          if (this.options['ad.expectedPattern'].pre) list = list.concat(this.options['ad.expectedPattern'].pre)
          if (this.options['ad.expectedPattern'].mid) list = list.concat(this.options['ad.expectedPattern'].mid)
          if (this.options['ad.expectedPattern'].post) list = list.concat(this.options['ad.expectedPattern'].post)
          if (list.length > 0) {
            var position = this.requestBuilder.lastSent.breakNumber
            if (position > list.length) position = list.length
            ret = list[position - 1]
          }
        } else {
          ret = this._adsAdapter.getExpectedAds()
        }
      }
    } catch (err) {
      Log.warn('An error occured while calling expectedAds', err)
    }
    return ret
  },

  getAdsExpected: function () {
    var ret = null
    try {
      ret = (this.getExpectedPattern() || this.getGivenAds()) || false
    } catch (err) {
      Log.warn('An error occured while calling givenAds or expectedPattern', err)
    }
    return ret
  },

  // ----------------------------------------- CHRONOS ------------------------------------------

  /**
   * Returns AdJoinDuration chrono delta time
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdJoinDuration: function () {
    return this._adsAdapter ? this._adsAdapter.chronos.join.getDeltaTime(false) : -1
  },

  /**
   * Returns AdBufferDuration chrono delta time
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdBufferDuration: function () {
    return this._adsAdapter ? this._adsAdapter.chronos.buffer.getDeltaTime(false) : -1
  },

  /**
   * Returns AdPauseDuration chrono delta time
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdPauseDuration: function () {
    return this._adsAdapter ? this._adsAdapter.chronos.pause.getDeltaTime(false) : 0
  },

  /**
   * Returns total totalAdDuration chrono delta time
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdTotalDuration: function () {
    return this._adsAdapter ? this._adsAdapter.chronos.total.getDeltaTime(false) : -1
  },

  getAdViewedDuration: function () {
    var maxTime = 0
    if (this._adsAdapter && this._adsAdapter.chronos.viewedMax.length > 0) {
      this._adsAdapter.chronos.viewedMax.forEach(function (chrono) {
        maxTime += chrono.getDeltaTime(false)
      })
    }
    return maxTime
  },

  getAdViewability: function () {
    var maxTime = 0
    if (this._adsAdapter && this._adsAdapter.chronos.viewedMax.length > 0) {
      this._adsAdapter.chronos.viewedMax.forEach(function (chrono) {
        if (chrono && chrono.getDeltaTime(false) > maxTime) { maxTime = chrono.getDeltaTime(false) }
      })
    }
    return maxTime
  },

  getAudioEnabled: function () {
    var ret = null
    try {
      if (this._adsAdapter) {
        ret = this._adsAdapter.getAudioEnabled()
      }
    } catch (err) {
      Log.warn('An error occured while calling isAudioEnabled', err)
    }
    return ret
  },

  getIsSkippable: function () {
    var ret = null
    try {
      if (this._adsAdapter) {
        ret = this._adsAdapter.getIsSkippable()
      }
    } catch (err) {
      Log.warn('An error occured while calling isSkippable', err)
    }
    return ret
  },

  getIsFullscreen: function () {
    var ret = null
    try {
      if (this._adsAdapter) {
        ret = this._adsAdapter.getIsFullscreen()
      }
    } catch (err) {
      Log.warn('An error occured while calling isFullcreen', err)
    }
    return ret
  }
}

module.exports = PluginAdsGettersMixin
