// No-Conflict
var previousYoubora = youbora
var youbora = {}

/**
 * This allows you to run multiple instances of YouboraLib on the same webapp.
 * After loading the new version, call `noConflict()` to get a reference to it.
 * At the same time the old version will be returned to Youbora.
 */
youbora.noConflict = function () {
  youbora = previousYoubora
  return this
}

// Info
youbora.VERSION = require('./version')

// Polyfills
youbora.polyfills = require('./polyfills')
youbora.polyfills()

// Base Classes
youbora.Object = require('./object')
youbora.Emitter = require('./emitter')

// Log
youbora.Log = require('./log')
youbora.Log.loadLevelFromUrl()

// General classes
youbora.Util = require('./util')
youbora.HybridNetwork = require('./monitors/hybridnetwork')
youbora.Chrono = require('./chrono')
youbora.Timer = require('./timer')
youbora.Constants = require('./constants')

// Comm classes
youbora.Request = require('./comm/request')
youbora.Communication = require('./comm/communication')

// Resource Transform classes
youbora.Transform = require('./comm/transform/transform')
youbora.ViewTransform = require('./comm/transform/view')
youbora.ResourceTransform = require('./comm/transform/resource')
youbora.CdnParser = require('./comm/transform/resourceparsers/cdnparser')
youbora.HlsParser = require('./comm/transform/resourceparsers/hlsparser')
youbora.OfflineParser = require('./comm/transform/offline')
youbora.LocationheaderParser = require('./comm/transform/resourceparsers/locationheaderparser')

// Plugin Classes
youbora.Options = require('./plugin/options')
youbora.Plugin = require('./plugin/plugin')
youbora.Storage = require('./plugin/storage')
youbora.RequestBuilder = require('./plugin/requestbuilder')

// Adapters
youbora.PlayheadMonitor = require('./adapter/playheadmonitor')
youbora.Adapter = require('./adapter/adapter')
youbora.adapters = {}

// Infinity
youbora.Infinity = require('./infinity/infinity')

// Detector classes
youbora.BackgroundDetector = require('./detectors/backgroundDetector')
youbora.DeviceDetector = require('./detectors/deviceDetector')
youbora.UUIDGenerator = require('./deviceUUID/hashgenerator')

/**
 * Register the given adapter in <youbora>.adapters.
 *
 * @param {string} key Unique adapter identifier.
 * @param {youbora.Adapter} Adapter Adapter class.
 *
 * @memberof youbora
 */
youbora.registerAdapter = function (key, Adapter) {
  this.adapters[key] = Adapter
}.bind(youbora)

/**
 * Remove the given adapter in <youbora>.adapters.
 *
 * @param {string} key Unique adapter identifier.
 *
 * @memberof youbora
 */
youbora.unregisterAdapter = function (key) {
  this.adapters[key] = null
}.bind(youbora)

module.exports = youbora
