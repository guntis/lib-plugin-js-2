var YouboraObject = require('../object')
/**
 * This static class provides screen scrolling and resizing detection methods.
 *
 * @class
 * @static
 * @memberof youbora
 */
var ResizeScrollDetector = YouboraObject.extend({
  constructor: function (plugin) {
    this.pluginref = plugin
    this.listenerReference = this._changeListener.bind(this)
    this.isEnabled = false
  },

  startDetection: function () {
    if (!this.isEnabled && typeof window !== 'undefined') {
      window.addEventListener('scroll', this.listenerReference)
      window.addEventListener('resize', this.listenerReference)
      this.isEnabled = true
    }
  },

  stopDetection: function () {
    if (this.isEnabled && typeof window !== 'undefined') {
      window.removeEventListener('scroll', this.listenerReference)
      window.removeEventListener('resize', this.listenerReference)
      this.isEnabled = false
    }
  },

  _changeListener: function () {
    var adsAdapter = this.pluginref.getAdsAdapter()
    if (adsAdapter && adsAdapter.flags.isStarted) {
      if (!adsAdapter.getIsVisible()) {
        adsAdapter.stopChronoView()
      } else {
        adsAdapter.startChronoView()
      }
    }
  }
})

module.exports = ResizeScrollDetector
