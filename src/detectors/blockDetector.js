var YouboraObject = require('../object')
var Util = require('../util')

var BlockDetector = YouboraObject.extend({
  constructor: function (plugin) {
    this._plugin = plugin
    this.isBlocked = null
    this.xhr = null
    try {
      if (XMLHttpRequest) {
        this.xhr = new XMLHttpRequest()
      } else {
        this.xhr = new ActiveXObject('Microsoft.XMLHTTP')
      }
    } catch (err) {
      return null
    }
    if (this.xhr.addEventListener) {
      var url = Util.addProtocol('ww.zxcvwwds.com/ww/wwds/-imwwge-wwd_wwds.html', this._plugin.options['app.https'])
      this.xhr.open('GET', url.replace(/ww/g, 'a'), false)
      this.xhr.addEventListener('load', this.notBlocked.bind(this))
      this.xhr.addEventListener('error', this.blocked.bind(this))
      this.xhr.send()
    } else {
      this.isBlocked = false
    }
  },

  blocked: function (e) {
    this.isBlocked = true
  },
  notBlocked: function (e) {
    this.isBlocked = false
  }
})

module.exports = BlockDetector
