var YouboraObject = require('../object')
/**
 * This static class provides device detection methods.
 *
 * @class
 * @static
 * @memberof youbora
 */
var BackgroundDetector = YouboraObject.extend({
  constructor: function (plugin) {
    this.plugin = plugin
    this.isInBackground = false
    this.listenerReference = this._visibilityListener.bind(this)
    this._reset()
  },

  startDetection: function () {
    if (!this.isBackgroundDetectorStarted && typeof document !== 'undefined') {
      this.isBackgroundDetectorStarted = true
      document.addEventListener('visibilitychange', this.listenerReference)
    }
  },

  stopDetection: function () {
    if (this.isBackgroundDetectorStarted && typeof document !== 'undefined') {
      document.removeEventListener('visibilitychange', this.listenerReference)
      this._reset()
    }
  },

  _reset: function () {
    this.isBackgroundDetectorStarted = false
  },

  _visibilityListener: function (e) {
    if (typeof document !== 'undefined') {
      var settings = this._getSettings()
      if (document.visibilityState === 'hidden') { // to Background
        this.isInBackground = true
        if (this.plugin && this.plugin._adsAdapter) {
          this.plugin._adsAdapter.stopChronoView()
        }
        if (typeof settings === 'string') {
          switch (settings) {
            case 'stop':
              this._fireStop()
              break
            case 'pause':
              this._firePause()
              break
          }
          if (this.plugin.storage.getLocal('infinityStarted') != null && !this.plugin.storage.getLocal('infinityStopped')) {
            this.lastBeatTime = new Date().getTime()
            var difftime = this.plugin._beat.chrono.startTime ? (this.lastBeatTime - this.plugin._beat.chrono.startTime) : 0
            this.plugin._sendBeat(difftime)
            this.plugin._beat.stop()
          }
        }
      } else if (document.visibilityState === 'visible') { // to Foreground
        this.isInBackground = false
        // ads
        if (this.plugin && this.plugin._adsAdapter) {
          this.plugin._adsAdapter.startChronoView()
        }
        // nothing for video yet
        if (typeof settings === 'string' && settings) {
          if (this.plugin.storage.getLocal('infinityStarted') != null && !this.plugin.storage.getLocal('infinityStopped')) {
            var now = new Date().getTime()
            if (now - this.lastBeatTime < this.plugin.sessionExpire) { // if session not expired
              this.plugin._sendBeat(now - this.lastBeatTime)
              this.plugin._beat.start()
            } else { // session expired
              this.plugin.restartViewTransform()
              this.plugin.infinity.begin()
            }
          }
        }
      }
    }
  },

  _getSettings: function () {
    if (typeof this.plugin.options['background.settings'] === 'string' && this.plugin.options['background.settings']) {
      return this.plugin.options['background.settings']
    }
    if (this.plugin.deviceDetector.isSmartTV()) {
      return this.plugin.options['background.settings.tv']
    }
    if (this.plugin.deviceDetector.isDesktop()) {
      return this.plugin.options['background.settings.desktop']
    }
    if (this.plugin.deviceDetector.isAndroid()) {
      return this.plugin.options['background.settings.android']
    }
    if (this.plugin.deviceDetector.isIphone()) {
      return this.plugin.options['background.settings.iOS']
    }
  },

  _firePause: function () {
    this.adsAdapter = this.plugin.getAdsAdapter()
    this.contentAdapter = this.plugin.getAdapter()
    if (this.adsAdapter && this.adsAdapter.flags.isStarted && !this.adsAdapter.flags.isEnded) {
      this.adsAdapter.firePause()
    } else if (this.contentAdapter && this.contentAdapter.flags.isStarted && !this.contentAdapter.flags.isEnded) {
      this.contentAdapter.firePause()
    }
  },

  _fireStop: function () {
    this.adsAdapter = this.plugin.getAdsAdapter()
    this.contentAdapter = this.plugin.getAdapter()
    if (this.adsAdapter && this.adsAdapter.flags.isStarted && !this.adsAdapter.flags.isEnded) {
      this.adsAdapter.fireStop()
    }
    this.plugin.fireStop()
  },

  canBlockStartCalls: function () {
    return (
      this.plugin.backgroundDetector.isInBackground &&
      this.plugin.options['background.enabled'] === true &&
      this.plugin.backgroundDetector._getSettings() // it is stop or pause
    )
  }
})

module.exports = BackgroundDetector
